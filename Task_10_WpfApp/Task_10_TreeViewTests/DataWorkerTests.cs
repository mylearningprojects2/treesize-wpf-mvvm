using Task_10_TreeView;

namespace Task_10_TreeViewTests
{
    [TestClass]
    public class DataWorkerTests
    {
        [TestMethod]
        [DataRow(1048576, (double)1)]
        [DataRow(500000, (double)0.476837158203125)]
        public void ConvertByteToMb_logic_Tests(long data, double expected)
        {

            double actual = DataWorker.ConvertByteToMb(data);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DataRow(1048576, (double)1)]
        [DataRow(500000, (double)0.48)]
        public void ConvertByteToMb_Round_Test(long data, double expected)
        {
            double actual = Math.Round(DataWorker.ConvertByteToMb(data),2);

            Assert.AreEqual(expected,actual);
        }
    }
}