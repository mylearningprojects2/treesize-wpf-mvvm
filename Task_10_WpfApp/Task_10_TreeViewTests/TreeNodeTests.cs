﻿using Task_10_TreeView;

namespace Task_10_TreeViewTests
{
    [TestClass]
    public class TreeNodeTests
    {
        //[TestMethod]
        //[DataRow(9000)]
        //public void SizePropTest(double expected)
        //{
        //    TreeNode testNode = new TreeNode()
        //    {
        //        Children =
        //        {
        //            new TreeNode()
        //            {
        //                Name="TestNode1",
        //                Items =
        //                {
        //                    new TreeItem() { Size = 1000 },
        //                    new TreeItem() { Size = 1000 },
        //                    new TreeItem() { Size = 1000 },
        //                    new TreeItem() { Size = 1000 },
        //                    new TreeItem() { Size = 1000 },
        //                }, 
        //                Children =
        //                {
        //                    new TreeNode()
        //                    {
        //                        Name = "SubTestNode2"  ,
        //                        Items =
        //                        {
        //                             new TreeItem() { Size = 1000 },
        //                             new TreeItem() { Size = 1000 },
        //                             new TreeItem() { Size = 1000 },
        //                             new TreeItem() { Size = 1000 },
        //                        }
        //                    }
        //                }
        //            },
        //        }
        //    };

        //    Assert.AreEqual(expected, testNode.Children.First(x=>x.Name== "TestNode1").Size);
        //}


        [TestMethod]
        [DynamicData(nameof(GetTestRoot), DynamicDataSourceType.Method)]
        public void SizeProp_SubFolder_Test(TreeNode testRoot, double[] expected)
        {

            Assert.AreEqual(expected[0], testRoot.Children.First(x => x.Name == "TestNode1").Size);

        }

        [TestMethod]
        [DynamicData(nameof(GetTestRoot), DynamicDataSourceType.Method)]
        public void SizeProp_SubSubFolder_Test(TreeNode testRoot, double[] expected)
        {

            Assert.AreEqual(expected[1], testRoot.Children.FirstOrDefault().Children.FirstOrDefault().Size);

        }

        [TestMethod]
        [DynamicData(nameof(GetTestRoot), DynamicDataSourceType.Method)]
        public void SizeProp_SubSubFolder_NotEqual_Test(TreeNode testRoot, double[] expected)
        {

            Assert.AreNotEqual(expected[2], testRoot.Children.FirstOrDefault().Children.FirstOrDefault().Size);

        }


        public static IEnumerable<object[]> GetTestRoot()
        {
            yield return new object[]
            {
                new TreeNode()
            {
                Children =
                {
                    new TreeNode()
                    {
                        Name="TestNode1",
                        Items =
                        {
                            new TreeItem() { Size = 1000 },
                            new TreeItem() { Size = 1000 },
                            new TreeItem() { Size = 1000 },
                            new TreeItem() { Size = 1000 },
                            new TreeItem() { Size = 1000 },
                        },
                        Children =
                        {
                            new TreeNode()
                            {
                                Name = "SubTestNode2"  ,
                                Items =
                                {
                                     new TreeItem() { Size = 1000 },
                                     new TreeItem() { Size = 1000 },
                                     new TreeItem() { Size = 1000 },
                                     new TreeItem() { Size = 1000 },
                                }
                            }
                        }
                    },
                }
            },
                new double [] {9000, 4000, 3999 }
            };
        }
    }
}
