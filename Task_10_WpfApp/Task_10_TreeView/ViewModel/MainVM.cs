﻿using Prism.Mvvm;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Task_10_TreeView
{
    internal class MainVM : BindableBase
    {
        #region Fields
        MainModel _model;
        private string _usedSpace = null;
        private List<string> _drivesList;
        private int _driveComboBoxSelectedIndex = -1;
        private ObservableCollection<TreeNode> _rootVM;
        //private string _folderSize;
        #endregion

        public MainVM()
        {
            _rootVM = new ObservableCollection<TreeNode>() { new TreeNode() };

            _model = new MainModel();

            _model.PropertyChanged += (s, e) => { RaisePropertyChanged(e.PropertyName); };
        }

        #region Properties

        public int MainThreadId
        {
            get { return _model.MainThreadId; }
        }

        public int AppLogicThreadId
        {
            get => _model.AppLogicThreadId;
        }

        public string UsedSpace
        {
            get
            {
                if (_model.Root != null)
                    _usedSpace = _model.Root[0].SizeView;
                return _usedSpace;
            }

            set => _usedSpace = value;
        }

        public List<string> DrivesList
        {
            get { return _drivesList = _model.DrivesList; }
        }

        public int DriveComboBoxSelectedIndex
        {
            get
            {
                return _model.DriveComboBoxSelectedIndex;
            }
            set
            {
                _driveComboBoxSelectedIndex = value;

                if (_driveComboBoxSelectedIndex != -1)
                    _model.DriveComboBoxSelectedIndex = value;
            }
        }

        public ObservableCollection<TreeNode> RootVM
        {
            get
            {
                if (_model.Root != null)
                    return _rootVM = _model.Root;

                return _rootVM;
            }
        }
        #endregion               
    }
}



