﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Task_10_TreeView
{
    internal class MainModel : BindableBase
    {
        private List<string> _drivesList;
        private DataWorker _dataWorker;
        private ObservableCollection<TreeNode> _root;
        private string _selectedDrive;
        private int _driveComboBoxSelectedIndex = -1;
        private int _appLogicThreadId;
        private int _mainThreadId;

        public MainModel()
        {
            _dataWorker = new DataWorker();

            DrivesList = _dataWorker.GetDrivesList();

            MainThreadId = Thread.CurrentThread.ManagedThreadId;
        }

        #region Properties
        public int TestMethodWork { get; set; }

        public int DriveComboBoxSelectedIndex
        {
            get => _driveComboBoxSelectedIndex;

            set
            {
                _driveComboBoxSelectedIndex = value;

                SelectedDrive = DrivesList[_driveComboBoxSelectedIndex];
            }
        }

        //Setter starts drive tree building and calculating
        public string SelectedDrive
        {
            get => _selectedDrive;

            set
            {
                _selectedDrive = value;

                BuildDriveTreeAsync(SelectedDrive);
            }
        }

        public List<string> DrivesList
        {
            get { return _drivesList; }
            set { _drivesList = value; RaisePropertyChanged("DriveList"); }
        }

        public int AppLogicThreadId
        {
            get => _appLogicThreadId;
            set
            {
                _appLogicThreadId = value;
                RaisePropertyChanged(nameof(AppLogicThreadId));
            }
        }

        public int MainThreadId
        {
            get => _mainThreadId;
            set
            {
                _mainThreadId = value;
                RaisePropertyChanged(nameof(MainThreadId));
            }
        }

        public ObservableCollection<TreeNode> Root
        {
            get { return _root; }
            set
            {
                _root = value;
                RaisePropertyChanged("RootVM");
            }
        }

        #endregion

        //MainLogic
        public TreeNode BuildDriveTree(TreeNode folder)
        {
            //Thread.Sleep(50);

            List<FileInfo> files = _dataWorker.GetFilesInfo(folder.Path);
            foreach (FileInfo file in files)
            {
                App.Current.Dispatcher.Invoke((Action)delegate
                {
                    folder.Items.Add(AddItem(file));
                    folder.ItemsCount++;
                });
            }

            folder.Size = folder.GetItemsSize();

            List<DirectoryInfo> directories = _dataWorker.GetDirectoryInfos(folder.Path);

            foreach (DirectoryInfo dir in directories)
            {
                App.Current.Dispatcher.Invoke((Action)delegate
                {
                    folder.Children.Add(AddChildren(dir));
                });
            }

            RecalcNodeProp(folder);

            RaisePropertyChanged("UsedSpace");

            foreach (TreeNode child in folder.Children)
                BuildDriveTree(child);

            return folder;
        }

        #region Private Methods
        private ObservableCollection<TreeNode> AddNewRoot(string driveName)
        {
            TreeNode root = new TreeNode() { Name = driveName, Path = driveName, IsExpanded = true };

            ObservableCollection<TreeNode> newRoot = new() { root };

            return newRoot;
        }

        private TreeNode AddChildren(DirectoryInfo directoryInfo)
        {
            return new TreeNode() { Name = directoryInfo.Name, Path = directoryInfo.FullName };
        }

        //TODO: подумати над покращенням. Можливо варто перенести у клас ТріНод.
        private TreeItem AddItem(FileInfo fileInfo)
        {
            return new TreeItem()
            {
                Name = fileInfo.Name,
                Path = fileInfo.FullName,
                Size = DataWorker.ConvertByteToMb(fileInfo.Length),
                SizeView = _dataWorker.FileSizeViewConverter(fileInfo.Length)
            };
        }

        private async void BuildDriveTreeAsync(string rootPath)
        {
            Root = AddNewRoot(rootPath);

            await Task.Run(() =>
            {
                AppLogicThreadId = Thread.CurrentThread.ManagedThreadId;

                BuildDriveTree(Root[0]);
            });
        }

        private void RecalcNodeProp(TreeNode folder)
        {
            for (int i = 0; i < folder.Children.Count; i++)
            {
                folder.Children[i].PropertyChanged += (s, e) =>
                {
                    if (e.PropertyName == nameof(folder.Size)) folder.Size = folder.GetNodeSizeMb();
                    if (e.PropertyName == nameof(folder.ItemsCount)) folder.ItemsCount = folder.GetAllItemsCount();
                };
            }
        }
        #endregion

    }
}
