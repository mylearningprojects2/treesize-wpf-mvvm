﻿using Prism.Mvvm;

namespace Task_10_TreeView
{
    public class TreeItem : BindableBase
    {
        private string _name;
        private string _path;
        private double _size;
        private string _sizeView;

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                RaisePropertyChanged(nameof(Name));
            }
        }

        public string Path
        {
            get => _path;
            set
            {
                _path = value;
                RaisePropertyChanged(nameof(Path));
            }
        }

        public double Size
        {
            get => _size;
            set
            {
                _size = value;
                RaisePropertyChanged(nameof(Size));
            }
        }

        public string SizeView
        {
            get => _sizeView;
            set
            {
                _sizeView = value;
                RaisePropertyChanged(nameof(SizeView));
            }
        }
    }
}
