﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Task_10_TreeView
{
    public class DataWorker
    {
        public List<string> GetDrivesList()
        {
            List<string> drives = Directory.GetLogicalDrives().ToList();

            return drives;
        }

        public List<FileInfo> GetFilesInfo(string path)
        {
            DirectoryInfo dir = new DirectoryInfo(path);

            List<FileInfo> files = new();

            try { files = dir.GetFiles().ToList(); }

            catch (UnauthorizedAccessException ex)
            {
                Debug.WriteLine("File Access Error: " + ex.Message);
            }

            return files;
        }

        public List<DirectoryInfo> GetDirectoryInfos(string path)
        {
            DirectoryInfo dir = new DirectoryInfo(path);
            List<DirectoryInfo> result = new();

            try
            {
                DirectoryInfo[] directoryInfos = dir.GetDirectories();
                foreach (DirectoryInfo directory in directoryInfos)
                {
                    if (dir.Exists)
                        result.Add(directory);
                }
            }
            catch (UnauthorizedAccessException ex) { Debug.WriteLine("Folder Access Error: " + ex.Message); }

            return result;
        }

        public string FileSizeViewConverter(long sizeBytes)
        {
            double size = 0;
            string unit = " byte";
            string result;

            bool kb = sizeBytes / 1000 > 0;
            bool mb = sizeBytes / 1000000 > 0;
            bool gb = sizeBytes / 1000000000 > 0;

            if (sizeBytes >= 0)
            {
                if (gb)
                {
                    size = (double)sizeBytes / 1073741824;
                    unit = " Gb";
                    result = $"{size:f2}" + unit;
                    return result;
                }

                if (mb)
                {
                    size = (double)sizeBytes / 1048576;
                    unit = " Mb";
                    result = $"{size:f2}" + unit;
                    return result;
                }

                if (kb)
                {
                    size = (double)sizeBytes / 1024;
                    unit = " Kb";
                    result = $"{size:f2}" + unit;
                    return result;
                }

                else
                {
                    result = $"{sizeBytes}" + unit;
                    return result;
                }
            }

            else return "Error";
        }

        public static double ConvertByteToMb(long sizeBytes)
        {
            double mbSize = Convert.ToDouble(sizeBytes) / 1048576;
            return mbSize;
        }

        public static double ConvertMbToGb(double sizeMb)
        {
            double gbSize = sizeMb / 1024;
            return Math.Round(gbSize, 2);
        }
    }
}


