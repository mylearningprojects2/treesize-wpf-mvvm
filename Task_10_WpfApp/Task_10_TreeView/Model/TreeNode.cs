﻿using Prism.Mvvm;
using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Windows.Data;

namespace Task_10_TreeView
{
    public class TreeNode : BindableBase
    {
        private double _size;
        private string _sizeView;
        private int _itemsCount;
        private ObservableCollection<TreeNode> _children;
        private ObservableCollection<TreeItem> _items;

        public TreeNode()
        {
            _children = new ObservableCollection<TreeNode>();
            _items = new ObservableCollection<TreeItem>();
            _size = 0;
            _sizeView = null;
            _itemsCount = Items.Count;
        }

        public string Name { get; set; }
        public double Size
        {
            get => _size;

            set
            {
                _size = value;

                RaisePropertyChanged(nameof(Size));

                SizeView = ConvertSizeToSizeView(Size);
            }
        }
        public string SizeView
        {
            get
            {
                _sizeView = ConvertSizeToSizeView(_size);
                return _sizeView;
            }
            set
            {
                _sizeView = value;
                RaisePropertyChanged(nameof(SizeView));
            }
        }
        public int ItemsCount
        {
            get => _itemsCount;
            set
            {
                _itemsCount = value;
                RaisePropertyChanged(nameof(ItemsCount));
            }
        }
        public string Path { get; set; }
        public bool IsExpanded { get; init; } = false;

        public ObservableCollection<TreeNode> Children
        {
            get => _children;

            set
            {
                _children = value;
                RaisePropertyChanged(nameof(Children));
            }
        }

        public ObservableCollection<TreeItem> Items
        {
            get => _items;
            set
            {
                _items = value;
                RaisePropertyChanged(nameof(Items));
            }
        }

        public IList Childrens => new CompositeCollection()
            {
                new CollectionContainer() { Collection = Children },
                new CollectionContainer() { Collection = Items }
            };

        public double GetNodeSizeMb()
        {
            double result = 0;

            result += GetItemsSize();

            result += GetChildrenSize();

            return result;
        }

        public double GetItemsSize()
        {
            double result = 0;
            foreach (TreeItem file in Items)
                result += file.Size;

            return result;
        }

        private double GetChildrenSize()
        {
            double result = 0;
            foreach (TreeNode folder in Children)
                result += folder.Size;

            return result;
        }

        private string ConvertSizeToSizeView(double size)
        {
            double oneGb = 1024;

            if (size > oneGb)
                return DataWorker.ConvertMbToGb(Math.Round(_size, 2)) + " Gb";

            return Math.Round(_size, 2) + " Mb";
        }

        private int GetChildrensItemsCount()
        {
            int count = 0;

            if (Children != null)
            {
                foreach (TreeNode child in Children)
                    count += child.ItemsCount;
            }

            return count;
        }

        public int GetAllItemsCount()
        {
            int count = 0;

            count = Items.Count + GetChildrensItemsCount();

            return count;
        }
    }
}
